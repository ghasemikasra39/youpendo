import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import ProfileComponent from "./ProfileComponent";

export default class RootComponent extends Component {
    render() {
        return (
           <View style={styles.container}>
               <ProfileComponent/>
           </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f7fb',
        alignSelf: 'stretch'
    }
});
