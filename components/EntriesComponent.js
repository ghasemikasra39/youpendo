import React, {Component, useState, useEffect} from "react";
import {
    FlatList,
    Image,
    ImageBackground,
    StyleSheet,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View
} from "react-native";
import * as Font from "expo-font";

export default function EntriesComponent() {
    const [DATA, setDATA] = useState([
        {id: '1', img: require('../assets/UI/image.png'), date: '16 JAN', location: 'Berlin, Germany'},
        {id: '2', img: require('../assets/UI/image2.png'), date: '15 JAn', location: 'Berlin, Germany'}
    ]);
    const [fontLoaded, setFontLoaded] = useState(false);

    async function loadFonts() {
        await Font.loadAsync({
            'Montserrat-SemiBold': require('../assets/fonts/Montserrat/Montserrat-SemiBold.ttf'),
            'Noteworthy': require('../assets/fonts/Noteworthy-Bold.ttf')
        });
        setFontLoaded(true);
    }

    useEffect(() => {
        loadFonts();
    });

    /**
     * Returns a single entry.
     *
     * @param {object} item An entry data.
     * @return {ReactElement}
     */
    function renderItem({item}) {
        return (
            <View>
                <TouchableOpacity
                    style={styles.TouchableOpacity}>
                    <Image style={styles.entryImg} source={item.img}/>
                    <View style={styles.entryInnerView}>
                        <Text style={styles.entryDate}>{item.date}</Text>
                        <Text style={styles.entryLocation}> {item.location}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={styles.entryContainer}>
            <ImageBackground source={require('../assets/UI/Rectangle.png')}
                             imageStyle={styles.rectangleImg}
                             style={styles.rectangle}
                             resizeMode="contain">
                <TouchableHighlight style={styles.recTouchableHighlight}>
                    <Image source={require("../assets/UI/Circle.png")}
                           style={styles.profileImg}/>
                </TouchableHighlight>
                {fontLoaded ? (<Text style={styles.entriesTitle}>Entries 2</Text>) : <></>}
                <FlatList
                    data={DATA}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                    numColumns={2}
                />
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    profileImg: {
        height: 120,
        width: 120
    },
    TouchableOpacity: {
        backgroundColor: '#ffffff',
        paddingTop: 10,
        paddingHorizontal: 10,
        marginHorizontal: 5,
        paddingBottom: 10,
        marginBottom: 10,
        borderRadius: 10,
        borderColor: '#ffffff',
        borderWidth: 1,
        elevation: 4,
        shadowOpacity: 0.4,
        shadowColor: 'gray',
        shadowOffset: {height: 4, width: 0},
    },
    entryImg: {
        width: 150,
        height: 200
    },
    entryDate: {
        fontWeight: 'bold',
        fontSize: 12
    },
    entryLocation: {
        fontWeight: 'bold',
        fontSize: 12,
        color: '#b2b5ba'
    },
    entryInnerView: {
        flexDirection: 'row',
        paddingTop: 5
    },
    rectangleImg: {height: 620, width: 800},
    rectangle: {width: 800, height: 620, alignItems: 'center'},
    recTouchableHighlight: {marginTop: -50, marginLeft: -270},
    entriesTitle: {
        fontFamily: 'Noteworthy',
        fontSize: 25,
        marginLeft: -280,
        marginTop: -10,
        marginBottom: 10
    },
    entryContainer: {paddingTop: 15, alignItems: 'center'}
});
