import React, {useState, useEffect} from 'react';
import {
    View,
    Text,
    StyleSheet,
    ImageBackground,
    ScrollView,
    TouchableHighlight,
    Image,
    Platform
} from 'react-native';
import {Button} from 'react-native-elements';
import * as Font from 'expo-font';
import EntriesComponent from "./EntriesComponent";


export default function ProfileComponent() {
    const [fontLoaded, setFontLoaded] = useState(false);
    const [btnTitle, setBtnTitle] = useState('Follow');
    const [btnClicked, setBtnClicked] = useState(false);

    async function loadFonts() {
        await Font.loadAsync({
            'Montserrat-SemiBold': require('../assets/fonts/Montserrat/Montserrat-SemiBold.ttf'),
            'Noteworthy': require('../assets/fonts/Noteworthy-Bold.ttf')
        });
        setFontLoaded(true);
    }

    useEffect(() => {
        loadFonts();
    });

    /**
     * handle button click.
     */
    function handleClick() {
        setBtnTitle('Following');
        setBtnClicked(true);
    }

    return (
        <ScrollView>
            <View style={styles.scrollView}>
                <ImageBackground source={require('../assets/UI/stars.png')}
                                 imageStyle={styles.starsImageStyle}
                                 style={styles.starsStyle}>
                    <TouchableHighlight
                        style={styles.profileImgContainer}>
                        <Image source={require("../assets/UI/Profile_pic.png")}
                               style={styles.profileImage}/>
                    </TouchableHighlight>
                    <View style={styles.profileText}>
                        {fontLoaded ? (<Text style={styles.profileName}>Ilias</Text>) : <></>}
                        {fontLoaded ? (
                            <Text style={styles.profileLocation}>Berlin, Germany</Text>) : <></>}
                        {fontLoaded ? (<Button
                            onPress={handleClick}
                            containerStyle={[styles.btnContainer,
                                {
                                    shadowColor: btnClicked ? 'white' : "#ec0f50",
                                    shadowOffset: Platform.OS === 'ios' ? {height: 2, width: 0} : null
                                }]}

                            buttonStyle={[styles.btn,
                                {
                                    backgroundColor: btnClicked ? 'white' : '#ec0f50',
                                    height: Platform.OS === 'ios' ? 40 : 35,
                                }]}
                            titleStyle={{
                                fontFamily: 'Montserrat-SemiBold',
                                color: btnClicked ? 'black' : 'white'
                            }}
                            title={btnTitle}/>) : <></>}
                    </View>
                </ImageBackground>
            </View>
            <EntriesComponent/>
        </ScrollView>
    )

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    profileImgContainer: {
        marginRight: 10,
        marginTop: 20,
        height: 150,
        width: 150,
        borderRadius: 75,
        overflow: 'hidden',
        elevation: 8,
        paddingBottom: 1,
        borderColor: 'white',
        borderWidth: 2,
    },
    profileImg: {
        height: 120,
        width: 120

    },
    TouchableOpacity: {
        backgroundColor: '#ffffff',
        paddingTop: 10,
        paddingHorizontal: 10,
        marginHorizontal: 5,
        paddingBottom: 10,
        marginBottom: 10,
        borderRadius: 10,
        borderColor: '#ffffff',
        borderWidth: 1,
        elevation: 4
    },
    starsImageStyle: {width: undefined, height: undefined},
    starsStyle: {height: 215, flexDirection: 'row'},
    scrollView: {padding: 15, paddingTop: 50},
    profileImage: {width: 150, height: 150},
    profileText: {
        flexDirection: 'column',
        paddingTop: 30,
        paddingLeft: 30,
        opacity: 1
    },
    profileName: {
        fontSize: 30,
        fontFamily: 'Montserrat-SemiBold',
        paddingTop: 10
    },
    profileLocation: {
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 15,
        paddingTop: 10,
        paddingBottom: 15,
        color: '#b2b5ba'
    },
    btnContainer: {
        paddingBottom: 3,
        elevation: 8,
        shadowOpacity: 0.8,
    },
    btn: {
        width: 120,
    }
});
